import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-department-confirm-delete',
  templateUrl: './department-confirm-delete.component.html',
  styleUrls: ['./department-confirm-delete.component.css']
})
export class DepartmentConfirmDeleteComponent implements OnInit {
  response: boolean;
  constructor(
    public dialogRef: MatDialogRef<DepartmentConfirmDeleteComponent>,
    @Inject(MAT_DIALOG_DATA) public department
  ) { }

  ngOnInit(): void {
  }

  setResponse(response: boolean) {
    this.dialogRef.close(response);
  }
}
