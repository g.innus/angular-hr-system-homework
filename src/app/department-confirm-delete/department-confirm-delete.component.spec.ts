import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartmentConfirmDeleteComponent } from './department-confirm-delete.component';

describe('DepartmentConfirmDeleteComponent', () => {
  let component: DepartmentConfirmDeleteComponent;
  let fixture: ComponentFixture<DepartmentConfirmDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepartmentConfirmDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartmentConfirmDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
