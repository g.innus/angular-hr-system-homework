import {Component, Input, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {Employee} from '../entyties/employee';
import {EmployeeService} from '../services/employee.service';
import {PositionService} from '../services/position.service';
import {DepartmentService} from '../services/department.service';
import {Position} from '../entyties/position';
import {Department} from '../entyties/department';
import {DepartmentConfirmDeleteComponent} from '../department-confirm-delete/department-confirm-delete.component';
import {EmployeeCreateComponent} from '../employee-create/employee-create.component';
import {ActivatedRoute} from '@angular/router';
import {forkJoin, Observable} from 'rxjs';



@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {
  selectedDepartmentId: number;
  selectedPositionId: number;
  selectedDepartment: Department;
  selectedPosition: Position;
  employees: Employee[] = [];
  positions: Position[] = [];
  departments: Department[] = [];
  displayedColumns: string[] = ['id', 'firstName', 'lastName', 'email', 'position', 'department', 'action'];

  constructor(
    private employeeService: EmployeeService,
    private positionService: PositionService,
    private departmentService: DepartmentService,
    public dialog: MatDialog,
    private activatedRoute: ActivatedRoute
  ) {
  }

  ngOnInit(): void {
    this.getAllData();
    this.activatedRoute.queryParams.subscribe(
      params => {
        if (params.department) {
          this.selectedDepartmentId = params.department;
        }
        if (params.position) {
          this.selectedPositionId = params.position;
        }
        if (!params.position && !params.department) {
          // Better to use null than undefined
          this.selectedPositionId = undefined;
          this.selectedDepartmentId = undefined;
          this.selectedDepartment = undefined;
          this.selectedPosition = undefined;
          this.getEmployees();
        }
        this.getAllData();
      }
    );
  }

  getEmployees(): Observable<Employee[]> {
    const observable = this.employeeService.getAll();
    observable.subscribe(rez => {
      this.employees = rez;

    });
    return observable;
  }

  getDepartments(): Observable<Department[]> {
    const observable = this.departmentService.getAll();
    observable.subscribe(rez => {
      this.departments = rez;
    });
    return observable;
  }

  getPositions(): Observable<Position[]> {
    const observable = this.positionService.getAll();
    observable.subscribe(rez => {
      this.positions = rez;
    });
    return observable;
  }


  getAllData() {
    // Please please please, never name variables with one character :)
    const e = this.getEmployees();
    const d = this.getDepartments();
    const p = this.getPositions();

    // nice solution with forkJoin!
    forkJoin([e, d, p]).subscribe(_ => {
      this.employees = this.addTitle(this.departments, this.employees, 'id', 'departmentId', 'department');
      this.employees = this.addTitle(this.positions, this.employees, 'id', 'positionId', 'position');

      if (this.selectedDepartmentId) {
        // when comparing try to stick with ===, to do a type-safe comparison
        this.employees = this.employees.filter(e => e.departmentId == this.selectedDepartmentId);
        this.selectedDepartment = this.departments.find(dep => dep.id == this.selectedDepartmentId);
      }
      if (this.selectedPositionId) {
        this.employees = this.employees.filter(e => e.positionId == this.selectedPositionId);
        this.selectedPosition = this.positions.find(pos => pos.id == this.selectedPositionId);
      }

    });
  }

  confirmDelete(employee: Employee) {
    const dialogRef = this.dialog.open(DepartmentConfirmDeleteComponent, {
      width: '250px',
      data: employee
    });
    dialogRef.afterClosed().subscribe(
      response => {
        if (response) {
          this.delete(employee);
        }
      });
  }

  delete(employee: Employee) {
    const observableResponse = this.employeeService.delete(employee.id);
    observableResponse.subscribe(
      rez => {
        this.ngOnInit();
      },
      error => alert('Unable to delete record. Possible foreign key in use')
    );
  }

  update(employee?: Employee) {
    let updateType = 'create';
    let observable;
    const emailsInUse = this.employees.map(e => e.email);
    if (employee) {
      updateType = 'update';
      const index = emailsInUse.indexOf(employee.email, 0);
      if (index > -1) {
        emailsInUse.splice(index, 1);
      }
    }
    const dialogRef = this.dialog.open(EmployeeCreateComponent, {
      width: '350px',
      data: {action: updateType, formData: employee, emailsInUse}
    });

    dialogRef.afterClosed().subscribe(
      rez => {
        if (rez) {
          if (rez.action === 'update') {
            observable = this.employeeService.update(rez.data);
          } else {
            observable = this.employeeService.create(rez.data);
          }
          observable.subscribe(
            next => this.ngOnInit()
            // usually when you do any create/update action it is worth notifying user that action has completed via alert or notification
          );
        }
      }
    );
  }

  // nice helper method, good job!
  private addTitle(addFrom: any[], addToo: any[], addToBy: string, addFromBy: string, newName: string) {
    const newArr = [];
    addToo.forEach(tooElement => {
      addFrom.filter(fromElement => fromElement[addToBy] === tooElement[addFromBy])
        .forEach(mach => {
          tooElement[newName] = mach.title;
          newArr.push(
            tooElement
          );
        });
    });
    return newArr;
  }
}
