import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {AbstractControl, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {DepartmentService} from '../services/department.service';

@Component({
  selector: 'app-department-create',
  templateUrl: './department-create.component.html',
  styleUrls: ['./department-create.component.css']
})
export class DepartmentCreateComponent implements OnInit {
  form: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<DepartmentCreateComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    private departmentService: DepartmentService,
  ) {
    this.form = this.formBuilder.group({
      id: new FormControl(),
      title: new FormControl('', [Validators.required, Validators.minLength(2)]),
    });
    this.form.controls.id.disable();
    if (data.action === 'create') {
      this.form.controls.id.setValue('Automatic');
    }
  }

  ngOnInit(): void {
    if (this.data.action === 'update') {
      this.form.patchValue(this.data.formData);
    }
  }

  submit() {
    const formValues = this.form.getRawValue();
    let updatedRecord;
    updatedRecord = formValues;
    if (this.data.action === 'create') {
      // it is always better to avoid using undefined value, null or optional can be used instead
      updatedRecord.id = undefined;
    }
    this.dialogRef.close({data: updatedRecord, action: this.data.action});
  }
}
