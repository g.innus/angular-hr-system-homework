export class Employee {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  departmentId: number;
  positionId: number;
  department: string;
  position: string;
}
