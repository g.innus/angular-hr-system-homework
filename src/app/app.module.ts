import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MaterialModule} from './material/material.module';
import { DepartmentsComponent } from './departments/departments.component';
import {HttpClientModule} from '@angular/common/http';
import { DepartmentCreateComponent } from './department-create/department-create.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { DepartmentConfirmDeleteComponent } from './department-confirm-delete/department-confirm-delete.component';
import { PositionComponent } from './position/position.component';
import { EmployeeComponent } from './employee/employee.component';
import { EmployeeCreateComponent } from './employee-create/employee-create.component';

@NgModule({
  declarations: [
    AppComponent,
    DepartmentsComponent,
    DepartmentCreateComponent,
    DepartmentConfirmDeleteComponent,
    PositionComponent,
    EmployeeComponent,
    EmployeeCreateComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
