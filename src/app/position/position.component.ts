import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {Position} from '../entyties/position';
import {DepartmentConfirmDeleteComponent} from '../department-confirm-delete/department-confirm-delete.component';
import {DepartmentCreateComponent} from '../department-create/department-create.component';
import {PositionService} from '../services/position.service';


@Component({
  selector: 'app-position',
  templateUrl: './position.component.html',
  styleUrls: ['./position.component.css']
})
export class PositionComponent implements OnInit {

  positions: Position[] = [];
  displayedColumns: string[] = ['id', 'title', 'action'];

  constructor(private positionService: PositionService, public dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.positionService.getAll().subscribe(result => {
      this.positions = result;
    });
  }

  delete(position: Position) {
    /* we usually do not save observable into a variable when it is not needed but do method chaining:
      this.positionService.delete(position.id).subscribe(result => {
        ...
      }...

     */
    let observableResponse = this.positionService.delete(position.id);
    observableResponse.subscribe(
      rez => {
        this.ngOnInit();
      },
      error => alert('Unable to delete record. Possible foreign key in use')
    );
  }

  confirmDelete(position: Position) {
    const dialogRef = this.dialog.open(DepartmentConfirmDeleteComponent, {
      width: '250px',
      data: position
    });
    dialogRef.afterClosed().subscribe(
      response => {
        if (response) {
          this.delete(position);
        }
      });
  }


  update(position ?: Position) {
    let updateType = 'create';
    let observable;
    if (position) {
      updateType = 'update';
    }
    const dialogRef = this.dialog.open(DepartmentCreateComponent, {
      width: '250px',
      data: {action: updateType, formData: position}
    });
    dialogRef.afterClosed().subscribe(
      rez => {
        if (rez) {
          if (rez.action === 'update') {
            observable = this.positionService.update(rez.data);
          } else {
            observable = this.positionService.create(rez.data);
          }
          observable.subscribe(
            next => this.ngOnInit()
          );
        }
      }
    );
  }
}
