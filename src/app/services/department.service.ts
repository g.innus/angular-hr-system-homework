import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Department} from '../entyties/department';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DepartmentService {

  constructor(private httpClient: HttpClient) {
  }

  /*
    Usually you should extract server DNS name and port to a variable, because backend applications address might change in future.
    Extracting server variable to an application configuration is a very good solution, read more here (https://devblogs.microsoft.com/premier-developer/angular-how-to-editable-config-files/)
  */
  getAll(): Observable<Department[]> {
    return this.httpClient.get<Department[]>('http://localhost:8080/departments');
  }

  delete(id: number): Observable<any> {
    return this.httpClient.delete('http://localhost:8080/departments/' + id)
      .pipe(
        catchError(this.handleError)
      );
  }
  update(department: Department){
    return this.httpClient.patch('http://localhost:8080/departments/', department)
      .pipe(
        catchError(this.handleError)
      );
  }
  create(department: Department){
    return this.httpClient.put('http://localhost:8080/departments/', department)
      .pipe(
        catchError(this.handleError)
      );
  }

  /*
  * For handling errors there is no need to copy the same method to each service, you can create a new service 'HttpErrorHandlerService' and move
  * this method there. This way you will have one error handler for all situations. Even more advanced technique- is creating your own HttpClient wrapper service
  * and doing error handling there. So in your services you have one-liners like "return this.httpService.put('http://localhost:8080/departments/', department);"
  * */
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      error.status);
  }
}
