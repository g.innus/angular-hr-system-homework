import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {Position} from '../entyties/position';
import {catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PositionService {

  constructor(private httpClient: HttpClient) {
  }

  getAll(): Observable<Position[]> {
    return this.httpClient.get<Position[]>('http://localhost:8080/positions');
  }

  delete(id: number): Observable<any> {
    return this.httpClient.delete('http://localhost:8080/positions/' + id)
      .pipe(
        catchError(this.handleError)
      );
  }
  update(position: Position){
    return this.httpClient.patch('http://localhost:8080/positions/', position)
      .pipe(
        catchError(this.handleError)
      );
  }
  create(position: Position){
    return this.httpClient.put('http://localhost:8080/positions/', position)
      .pipe(
        catchError(this.handleError)
      );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      error.status);
  }
}
