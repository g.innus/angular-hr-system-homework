import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Employee} from '../entyties/employee';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private httpClient: HttpClient) {
  }

  private static handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError(
      error.status);
  }

  getAll(): Observable<Employee[]> {
    return this.httpClient.get<Employee[]>('http://localhost:8080/employees');
  }

  delete(id: number): Observable<any> {
    return this.httpClient.delete('http://localhost:8080/employees/' + id)
      .pipe(
        catchError(EmployeeService.handleError)
      );
  }
  update(employee: Employee){
    return this.httpClient.patch('http://localhost:8080/employees/', employee)
      .pipe(
        catchError(EmployeeService.handleError)
      );
  }
  create(employee: Employee){
    return this.httpClient.put('http://localhost:8080/employees/', employee)
      .pipe(
        catchError(EmployeeService.handleError)
      );
  }
}
