import {Component, OnInit} from '@angular/core';
import {DepartmentService} from '../services/department.service';
import {Department} from '../entyties/department';
import {Observable, observable} from 'rxjs';
import {MatDialog} from '@angular/material/dialog';
import {DepartmentCreateComponent} from '../department-create/department-create.component';
import {DepartmentConfirmDeleteComponent} from '../department-confirm-delete/department-confirm-delete.component';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-departments',
  templateUrl: './departments.component.html',
  styleUrls: ['./departments.component.css']
})
export class DepartmentsComponent implements OnInit {
  selectedDepartmentId: number;
  departments: Department[] = [];
  displayedColumns: string[] = ['id', 'title', 'action'];

  constructor(
    public dialog: MatDialog,
    private activatedRoute: ActivatedRoute,
    private departmentService: DepartmentService
  ) {
  }

  ngOnInit(): void {
    this.departmentService.getAll().subscribe(result => {
      this.departments = result;
    });
  }

  delete(department: Department) {
    let observableResponse = this.departmentService.delete(department.id);
    observableResponse.subscribe(
      rez => {
        this.ngOnInit();
      },
        /* Default alerts look pretty bad, you can use a material dialog or a library like https://www.npmjs.com/package/angular2-toaster to show
        * some fancy notifications */
      error => alert('Unable to delete record. Possible foreign key in use')
    );
  }

  confirmDelete(department: Department) {
    const dialogRef = this.dialog.open(DepartmentConfirmDeleteComponent, {
      width: '350px',
      data: department
    });
    dialogRef.afterClosed().subscribe(
      response => {
        if (response) {
          this.delete(department);
        }
      });
  }


  update(department ?: Department) {
    let updateType = 'create';
    let observable;
    if (department) {
      updateType = 'update';
    }
    const dialogRef = this.dialog.open(DepartmentCreateComponent, {
      width: '250px',
      data: {action: updateType, formData: department}
    });

    dialogRef.afterClosed().subscribe(
      rez => {
        if (rez) {
          if (rez.action === 'update') {
            observable = this.departmentService.update(rez.data);
          } else {
            observable = this.departmentService.create(rez.data);
          }
          observable.subscribe(
            next => this.ngOnInit()
          );
        }
      }
    );
  }


}
