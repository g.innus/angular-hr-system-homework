import {Component, Inject, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {EmployeeService} from '../services/employee.service';
import {DepartmentService} from '../services/department.service';
import {PositionService} from '../services/position.service';
import {Department} from '../entyties/department';
import {Position} from '../entyties/position';
import {Employee} from '../entyties/employee';

@Component({
  selector: 'app-employee-create',
  templateUrl: './employee-create.component.html',
  styleUrls: ['./employee-create.component.css']
})
export class EmployeeCreateComponent implements OnInit {

  form: FormGroup;
  departments: Department[] = [];
  positions: Position[] = [];
  employees: Employee[] = [];

  // anything that is injected via constructor should have private scope, this is done to avoid errors with data changes from other components
  constructor(
    public dialogRef: MatDialogRef<EmployeeCreateComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    private employeeService: EmployeeService,
    private  departmentService: DepartmentService,
    private  positionService: PositionService,
  ) {
    this.form = this.formBuilder.group({
      id: new FormControl(),
      firstName: new FormControl('',
        [
          Validators.required,
          Validators.minLength(2)]),
      lastName: new FormControl('',
        [
          Validators.required,
          Validators.minLength(2)]),
      email: new FormControl('',
        [
          this.uniqueEmailValidator(data.emailsInUse),
          Validators.required,
          Validators.email,
          Validators.maxLength(50)]),
      departmentId: new FormControl('', Validators.required),
      positionId: new FormControl('', Validators.required),
    });
    this.form.controls.id.disable();
    if (data.action === 'create') {
      this.form.controls.id.setValue('Automatic');
    }
  }

  ngOnInit(): void {
    this.departmentService.getAll().subscribe(result => {
      this.departments = result;
    });
    this.positionService.getAll().subscribe(result => {
      this.positions = result;
    });
    this.employeeService.getAll().subscribe(result => {
      this.employees = result;
    });
    if (this.data.action === 'update') {
      this.form.patchValue(this.data.formData);
    }
  }

  submit() {
    const employee: Employee = this.form.getRawValue();
    if (this.data.action === 'create') {
      employee.id = undefined;
    }
    this.dialogRef.close({data: employee, action: this.data.action});
  }

  /* We usually move validators to a separate service, so they can be re-used in all forms */
  uniqueEmailValidator(emails: string[]): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const forbidden = emails.includes(control.value);
      return forbidden ? {unique: {unique: control.value}} : null;
    };
  }
}
